package devcraft.parking;

import java.time.*;
import java.time.temporal.ChronoField;

import static java.time.temporal.ChronoUnit.DAYS;

abstract class  WeekdaysPaymentPolicy implements PaymentPolicy {


    public int calcPayment(Instant entryTime, Instant paymentTime, ZoneId zoneId) {

        Instant next6am = next6am(entryTime, ZoneOffset.UTC);

        int amount = 0;
        while (!next6am.isAfter(paymentTime)) {
            amount += getPaymentPolicy(entryTime).calcPayment(entryTime, next6am, ZoneOffset.UTC);
            entryTime = next6am;
            next6am = next6am.plus(1, DAYS);
        }

        if (entryTime.isBefore(paymentTime))
            amount += getPaymentPolicy(entryTime).calcPayment(entryTime, paymentTime, ZoneOffset.UTC);

        return amount;
    }

    PaymentPolicy getPaymentPolicy(Instant entryTime) {
        ZonedDateTime zonedEntryTime = ZonedDateTime.ofInstant(entryTime, ZoneOffset.UTC);
        DayOfWeek dayOfWeek = DayOfWeek.of(zonedEntryTime.get(ChronoField.DAY_OF_WEEK));
        return getPaymentPolicy(dayOfWeek);
    }

    abstract protected PaymentPolicy getPaymentPolicy(DayOfWeek dayOfWeek);


    Instant next6am(Instant entryTime, ZoneOffset zone) {
        ZonedDateTime zonedEntryTime = ZonedDateTime.ofInstant(entryTime, zone);
        LocalTime sixAm = LocalTime.of(6, 0);
        LocalDate dayOfEntry = zonedEntryTime.toLocalDate();
        ZonedDateTime next6am = ZonedDateTime.of(dayOfEntry, sixAm, ZoneOffset.UTC);

        if (!next6am.isAfter(zonedEntryTime))
            next6am = next6am.plusDays(1);
        return next6am.toInstant();
    }

}