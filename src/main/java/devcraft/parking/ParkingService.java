package devcraft.parking;


import devcraft.parking.jpa.ParkingEntryRepository;

import java.time.Duration;
import java.time.Instant;

import static java.time.ZoneOffset.UTC;

public class ParkingService {

    private ParkingEntryRepository repository;
    private Clock clock;
    private PaymentCalculator paymentCalculator = new PaymentCalculator();

    public interface Clock {
        long now();
    }

    public ParkingService(Clock clock, ParkingEntryRepository repository) {
        this.clock = clock;
        this.repository = repository;
        paymentCalculator.setEscapeTime(Duration.ofMinutes(10));
    }

    public long enterParking() {
        ParkingEntryRepository.ParkingEntry entry = new ParkingEntryRepository.ParkingEntry();
        entry.setTime(clock.now());
        repository.save(entry);
        return entry.getCode();
    }

    public int calcPayment(long code) {
        ParkingEntryRepository.ParkingEntry entry = repository.findOne(code);
        long time = entry.getTime();
        return paymentCalculator.build().calcPayment(Instant.ofEpochMilli(time), Instant.ofEpochMilli(clock.now()), UTC);
    }

}