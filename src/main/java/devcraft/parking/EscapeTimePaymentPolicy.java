package devcraft.parking;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

class EscapeTimePaymentPolicy implements PaymentPolicy {

    private Duration escapeTime;
    private PaymentPolicy delegate;

    public EscapeTimePaymentPolicy(Duration escapeTime, PaymentPolicy delegate) {
        this.escapeTime = escapeTime;
        this.delegate = delegate;
    }

    @Override
    public int calcPayment(Instant entryTime, Instant paymentTime, ZoneId zoneId) {
        Duration timeInParking = Duration.between(entryTime, paymentTime);
        if (timeInParking.toMillis() < escapeTime.toMillis()) {
            return 0;
        }
        return delegate.calcPayment(entryTime,paymentTime,zoneId);
    }
}
