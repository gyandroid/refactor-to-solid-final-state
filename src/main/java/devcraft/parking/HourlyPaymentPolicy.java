package devcraft.parking;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

class HourlyPaymentPolicy implements PaymentPolicy {
    private Duration firstPaymentInterval = Duration.ofMinutes(60);
    private int firstIntervalRate = 12;
    private long paymentInterval = minAsMillis(15);
    private int intervalRate = 3;

    public int calcPayment(Instant entryTime, Instant paymentTime, ZoneId zoneId) {
        Duration timeInParking = Duration.between(entryTime, paymentTime);
        int amountToPay = this.firstIntervalRate;
        if (timeInParking.toMillis() >= firstPaymentInterval.toMillis()) {
            long intervalsToPay = calcIntervalsToPay(timeInParking.toMillis());
            amountToPay += (intervalsToPay * intervalRate);
        }
        return amountToPay;
    }

    private long calcIntervalsToPay(long timeInParking) {
        timeInParking -= firstPaymentInterval.toMillis();
        timeInParking += paymentInterval;
        return timeInParking / paymentInterval;
    }

    private long minAsMillis(int min) {
        return min * 60 * 1000L;
    }
}
