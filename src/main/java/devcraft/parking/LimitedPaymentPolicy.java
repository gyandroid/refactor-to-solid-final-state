package devcraft.parking;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

class LimitedPaymentPolicy implements PaymentPolicy {
    private final int maxDailyPayment = 96;
    private PaymentPolicy target;

    public LimitedPaymentPolicy(PaymentPolicy target) {
        this.target = target;
    }

    @Override
    public int calcPayment(Instant entryTime, Instant paymentTime, ZoneId zoneId) {
        int amountToPay = target.calcPayment(entryTime, paymentTime, zoneId);
        return Math.min(amountToPay, maxDailyPayment);
    }

}
