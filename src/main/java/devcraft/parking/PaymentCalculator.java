package devcraft.parking;

import java.time.*;

public class PaymentCalculator {

    private Duration escapeTime = Duration.ZERO;
    private int weekendDayFixedRate = 40;

    public void setEscapeTime(Duration escapeTime) {
        this.escapeTime = escapeTime;
    }

    public PaymentPolicy build() {
        return new EscapeTimePaymentPolicy(escapeTime, new WeekdaysPaymentPolicy(){
            protected PaymentPolicy getPaymentPolicy(DayOfWeek dayOfWeek) {
                PaymentPolicy paymentPolicy = null;
                if (isWeekendDay(dayOfWeek)){
                    paymentPolicy = new LimitedPaymentPolicy(new DayAndNightPaymentPolicy(
                            LocalTime.of(22,0),new HourlyPaymentPolicy(), new FixedPricePolicy(weekendDayFixedRate)));
                } else
                 paymentPolicy = new LimitedPaymentPolicy(new HourlyPaymentPolicy());
                return paymentPolicy;
            }
        });
    }

    private boolean isWeekendDay(DayOfWeek dayOfWeek) {
        return dayOfWeek.getValue() >=DayOfWeek.TUESDAY.getValue() && dayOfWeek.getValue() <= DayOfWeek.SATURDAY.getValue();
    }
}
