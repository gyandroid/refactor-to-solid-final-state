package devcraft.parking;

import org.junit.Assert;
import org.junit.Test;

import java.time.*;
import java.time.temporal.ChronoUnit;

import static java.time.DayOfWeek.*;
import static java.time.ZoneOffset.UTC;
import static java.time.temporal.TemporalAdjusters.next;

public class PaymentTest {
    PaymentCalculator calculator = new PaymentCalculator();

    LocalDateTime sundayAt6am;
    LocalDateTime mondayAt6am;
    LocalDateTime tuesdayAt6am;
    LocalDateTime wednesdayAt6am;
    LocalDateTime thursdayAt6am;
    LocalDateTime fridayAt6am;
    LocalDateTime saturdayAt6am;

    {
        sundayAt6am = LocalDateTime.of(2018, Month.MAY, 1, 6, 0).with(next(SUNDAY));
        mondayAt6am = sundayAt6am.with(next(MONDAY));
        tuesdayAt6am = sundayAt6am.with(next(TUESDAY));
        wednesdayAt6am = sundayAt6am.with(next(WEDNESDAY));
        thursdayAt6am = sundayAt6am.with(next(THURSDAY));
        fridayAt6am = sundayAt6am.with(next(FRIDAY));
        saturdayAt6am = sundayAt6am.with(next(SATURDAY));
    }

    private Instant timeFrom(long epochMilli, LocalDateTime from) {
        return from.plus(epochMilli, ChronoUnit.MILLIS).toInstant(ZoneOffset.UTC);
    }

    @Test
    public void shouldPayNothingOnFirst10Min() {
        Assert.assertEquals(0, calculator.build().calcPayment(
                timeFrom(0, sundayAt6am),
                timeFrom(0 * 60 * 1000L - 1, sundayAt6am), UTC));
    }


    @Test
    public void shouldPayHourRateOn10MinMark() {
        Assert.assertEquals(12, calculator.build().calcPayment(timeFrom(0, sundayAt6am), timeFrom(10 * 60 * 1000L, sundayAt6am), UTC));
    }

    @Test
    public void shouldPay15onTheHourMark() {
        Assert.assertEquals(15, calculator.build().calcPayment(timeFrom(0, sundayAt6am), timeFrom(60 * 60 * 1000L, sundayAt6am), UTC));
    }

    @Test
    public void shouldPay18onTheHourAnd15Mark() {
        Assert.assertEquals(18, calculator.build().calcPayment(timeFrom(0, sundayAt6am), timeFrom(75 * 60 * 1000L, sundayAt6am), UTC));
    }

    @Test
    public void shouldPay96Before8HoursMark() {
        Assert.assertEquals(96, calculator.build().calcPayment(timeFrom(0, sundayAt6am), timeFrom(8 * 60 * 60 * 1000L - 1, sundayAt6am), UTC));
    }

    @Test
    public void shouldPay96On8HoursMark() {
        Assert.assertEquals(96, calculator.build().calcPayment(timeFrom(0, sundayAt6am), timeFrom(8 * 60 * 60 * 1000L, sundayAt6am), UTC));
    }

    @Test
    public void shouldStartNewPaymentAt6am() {
        Assert.assertEquals(12 * 2, calculator.build().calcPayment(timeFrom(-1, sundayAt6am), timeFrom(10 * 60 * 1000L, sundayAt6am), UTC));
    }

    @Test
    public void shouldStartNewPaymentEveryDay() {
        Assert.assertEquals(96 * 3, calculator.build().calcPayment(timeFrom(2, sundayAt6am), timeFrom(3 * 24 * 60 * 60 * 1000L, sundayAt6am), UTC));
    }

    @Test
    public void shouldPayFixedRateOnWeekendNights() {
        Assert.assertEquals(40, calculator.build().calcPayment(timeFrom(16 * 60 * 60 * 1000L, thursdayAt6am),
                timeFrom(16 * 60 * 60 * 1000L + 10 * 60 * 1000L, thursdayAt6am), UTC));
    }

    @Test
    public void shouldPayHourlyRateBeforeNightStartAndFixedRateAfter() {
        Assert.assertEquals(52, calculator.build().calcPayment(timeFrom(16 * 60 * 60 * 1000L-1, thursdayAt6am),
                timeFrom(16 * 60 * 60 * 1000L + 10 * 60 * 1000L, thursdayAt6am), UTC));
    }

}